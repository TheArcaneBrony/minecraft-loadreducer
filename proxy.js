const McProxy = require('basic-minecraft-proxy');

let localServerOptions = {
    'port': '25565',
    'version': '1.16.1',
    'online-mode': true,
    'motd': 'nodejs minecraft proxy'
}

let serverList = {
    queue: {
        host: 'localhost',
        port: 25566,
        isDefault: true
    },
    server: {
        host: 'localhost',
        port: 25567,
        isFallback: true
    }
}

let mainServerState = 0;
// if you leave proxyOptions empty yo may as well not pass it in the arguments, I wrote it anyway to point out that it exist
let proxyOptions = {
  // the proxy won't automatically connect players to the default server
  autoConnect: true,
  // the proxy won't automatically move players to the fallback server when they get kicked
  autoFallback: true
}

/*
  Use the "/server <serverName>" command in chat to move between servers.
  <serverName> is the name that you chose for the server inside the serverList
  This command is implemented by /src/Plugins/ChatCommands.js and it can be disabled by setting enablePlugin: false inside proxyOptions
*/
let proxy = McProxy.createProxy(localServerOptions, serverList, proxyOptions);

proxy.on('error', console.error);

proxy.on('listening', () => {
    console.info('Listening!');
});
proxy.on('login', (player) => {
    console.info(`${player.username} connected from ${player.socket.remoteAddress}`)
    if (mainServerState === 0) {
        //start server
        //proxy.setRemoteServer(client.id, 'queue')
    }
    else if (mainServerState === 1) {
        //notify starting, send log
        proxy.setRemoteServer(client.id, 'queue')
    }
    else if (mainServerState === 2) {
        //server started, connect
        proxy.setRemoteServer(client.id, 'server')
    }

    player.on('end', () => {
        console.info(`${player.username} disconnected: ${player.socket.remoteAddress}`)
    })

    player.on('error', (err) => {
        console.error(`${player.username} disconnected with error: ${player.socket.remoteAddress}`, err)
    })
})